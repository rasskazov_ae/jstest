export default class Lesson {
    spanRc: JQuery<HTMLElement>;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    a: number;
    b: number;
    constructor(node: JQuery<HTMLElement>,
                canvasElement: HTMLCanvasElement,
                canvasContext: CanvasRenderingContext2D,
                startBtn: JQuery<HTMLElement>) {
        this.spanRc = node;
        this.canvas = canvasElement;
        this.context = canvasContext;

        startBtn.click(this.setValues);
    }

    setValues = () => {
        $("#sprite div").remove();
        $("#firstArrow").empty();
        this.a = Math.floor(Math.random() * 4 + 6);
        this.b = Math.floor(Math.random() * 4 + (11 - this.a));
        $("#ac")[0].textContent = this.a.toString();
        $("#bc")[0].textContent = this.b.toString();
        this.spanRc[0].textContent = "?";
        this.drawBg(this.draw);
    };

    draw = (f: number, s: number, id: string) => {
        if (f == undefined) {
            f = 0;
            s = Number($("#ac").text());
            id = "ac";
        }

        let length = s * 39;
        let x0 = 35 + f * 39, y0 = 92;
        let x1 = x0 + length / 2, y1 = y0 - length / 2;
        let x2 = length + x0, y2 = y0;
        let x = 3 + f * 39 * 100 / 875 + s * 38.8 * 50 / 875;
        let radius = length / Math.sqrt(2);
        let alpha = 2 * Math.asin(length / (2 * radius));
        let h = radius * (1 - Math.cos(alpha / 2));
        let y = 58 * 100 / 150 + h * 100 / 150;
        this.context.beginPath();
        this.context.strokeStyle = "red";
        this.context.lineWidth = 1;
        this.context.moveTo(x0, y0);
        this.context.arcTo(x1, y1, x2, y2, radius);
        this.context.lineTo(x2 - 10, y2 - 4);
        this.context.moveTo(x2, y2);
        this.context.lineTo(x2 - 4, y2 - 10);
        this.context.stroke();
        this.crInput(x, y, id);

    };

    inputCheck = (event: JQuery.Event<HTMLElement>) => {
        let input = event.target as HTMLInputElement;
        let id = "#" + event.target.id.slice(0, -2);
        let element = $(id);
        let num = element.text();
        if (!/[1-9]/g.test(input.value)) {
            input.value = "";
            return;
        }
        if (input.value == num) {
            let span = $("<span>").text(input.value).css("color", "black");
            $(event.target).replaceWith(span);
            if (element.hasClass("help")) {
                element.removeClass("help");
            }
            if (id == "#bc") {
                this.crInputRes();
                return;
            }
            this.draw(this.a, this.b, "bc");
            return;
        }
        $(input).css("color", "red");
        element.addClass("help");
        return;
    };

    crInput = (x: number, y: number, id: string) => {
        let input = $("<input>")
            .attr("id", id + "ie")
            .attr("type", "text")
            .attr("maxlength", "1")
            .attr("class", "form-control");
        input.keyup(this.inputCheck);
        var div = $("<div>")
            .css("bottom", y + "%")
            .css("left", x + "%");
        div.append(input);
        $("#sprite").append(div);
        return;
    };

    crInputRes = () => {
        $("#rcContainer")
            .removeClass("col-4")
            .addClass("col-8");
        let input = $("<input>")
            .addClass(this.spanRc[0].className)
            .addClass("form-control")
            .attr("id", this.spanRc[0].id)
            .attr("maxlength", "2");
        input.keyup(this.checkResult);
        this.spanRc.replaceWith(input);
    };

    checkResult = (event: JQuery.Event<HTMLElement>) => {
        let res = this.a + this.b;
        let inputTarget = event.target as HTMLInputElement;
        if (inputTarget.value !== res.toString()) {
            $(event.target).css("color", "red");
            return;
        }
        $("#rcContainer")
            .removeClass("col-8")
            .addClass("col-4");
        var input = $("#rc");
        this.spanRc.text(res);
        input.replaceWith(this.spanRc);
        return;
    };

    drawBg = (callback?: (f: number, s: number, id: string) => void) => {
        this.canvas.width = 875;
        this.canvas.height = 150;
        var bg = new Image();
        bg.src = "./images/sprite.png";
        bg.onload = () => {
            this.context.drawImage(bg, 0, this.canvas.height / 2, this.canvas.width, this.canvas.height / 2);
        };
        if (callback) {
            setTimeout(callback, 200);
        }
        return;
    };
}