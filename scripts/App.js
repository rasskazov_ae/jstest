define(["require", "exports", "./Lesson"], function (require, exports, Lesson_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class MyApp {
        constructor() {
            this.main = () => {
                let btn = $("#startBtn");
                let element = document.getElementById("firstArrow");
                if (element) {
                    let canvas = element;
                    let context = canvas.getContext("2d");
                    if (context) {
                        let lesson = new Lesson_1.default($("#rc"), canvas, context, btn);
                        this.run(lesson);
                    }
                }
            };
            this.run = (lesson) => {
                lesson.drawBg();
            };
        }
    }
    let app = new MyApp();
    $(document).ready(app.main);
});
