import Lesson from "./Lesson";

class MyApp {
    main = () => {
        let btn = $("#startBtn");
        let element = document.getElementById("firstArrow");
        if (element) {
            let canvas = element as HTMLCanvasElement
            let context = canvas.getContext("2d")
            if (context) {
                let lesson = new Lesson($("#rc"), canvas, context, btn);
                this.run(lesson);
            }
        }
    }

    run = (lesson: Lesson) => {
        lesson.drawBg();
    }
}

let app = new MyApp();
$(document).ready(app.main);