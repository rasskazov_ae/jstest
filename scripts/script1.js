var Lesson = (function () {
	var _this;
	function Lesson() {
		this.spanRc = $('#rc');
		
		_this = this;
	}
	
	Lesson.prototype.setValues = function () {
		$('#sprite div').remove();
		$('#firstArrow').empty();
		a = Math.floor(Math.random() * 4 + 6);
		b = Math.floor(Math.random() * 4 + (11 - a));
		$('#ac')[0].textContent = a;
		$('#bc')[0].textContent = b;
		_this.spanRc[0].textContent = '?';
		_this.drawBg(_this.draw);
	};
	
	Lesson.prototype.draw = function (f, s, id) {
		if (f == undefined) {
			f = 0;
			s = $('#ac')[0].textContent;
			id = 'ac';
		}
		var canvas = document.getElementById('firstArrow');
		var c = canvas.getContext('2d');
		var length = s * 39;
		var x0 = 35 + f * 39, y0 = 92;
		var x1 = x0 + length / 2, y1 = y0 - length / 2;
		var x2 = length + x0, y2 = y0;
		var x = 3 + f * 39 * 100 / 875 + s * 38.8 * 50 / 875;
		var radius = length / Math.sqrt(2);
		var alpha = 2 * Math.asin(length / (2 * radius));
		var h = radius * (1 - Math.cos(alpha / 2));
		var y = 58 * 100 / 150 + h * 100 / 150;
		c.beginPath();
		c.strokeStyle = 'red';
		c.lineWidth = 1;
		c.moveTo(x0, y0);
		c.arcTo(x1, y1, x2, y2, radius);
		c.lineTo(x2 - 10, y2 - 4);
		c.moveTo(x2, y2);
		c.lineTo(x2 - 4, y2 - 10);
		c.stroke();
		_this.crInput(x, y, id);
		return;
	};
	
	Lesson.prototype.inputCheck2 = function (event) {
		var x = '#' + event.target.id.slice(0, -2);
		var a = $(x)[0].textContent;
		if (!/[1-9]/g.test(event.target.value)) {
			event.target.value = '';
			return;
		}
		if (event.target.value == a) {
			var input = $(event.target);
			var span = $('<span>').text(input.val()).css('color', 'black');
			input.replaceWith(span);
			if ($(x).hasClass('help')) {
				$(x).removeClass('help');
			}
			if (x == '#bc') {
				_this.crInputRes();
				return;
			}
			_this.draw(a, b, 'bc');
			return;
		}
		$(event.target).css('color', 'red');
		$(x).addClass('help');
		return;
	};
	
	Lesson.prototype.crInput = function (x, y, id) {
		var input = $('<input>')
			.attr('id', id + 'ie')
			.attr('type', 'text')
			.attr('maxlength', '1')
			.attr('class', 'form-control');
		input.keyup(_this.inputCheck2);
		var div = $('<div>')
			.css('bottom', y + '%')
			.css('left', x + '%');
		div.append(input);
		$('#sprite').append(div);
		return;
	};
	
	Lesson.prototype.crInputRes = function () {
		$('#rcContainer')
			.removeClass('col-4')
			.addClass('col-8');
		var input = $('<input>')
			.addClass(_this.spanRc[0].className)
			.addClass('form-control')
			.attr('id', _this.spanRc[0].id)
			.attr('maxlength', '2');
		input.keyup(_this.checkResult);
		_this.spanRc.replaceWith(input);
	};
	
	Lesson.prototype.checkResult = function (event) {
		var res = a + b;
		if (event.target.value != res) {
			$(event.target).css('color', 'red');
			return;
		}
		$('#rcContainer')
			.removeClass('col-8')
			.addClass('col-4');
		var input = $('#rc');
		_this.spanRc[0].textContent = res;
		input.replaceWith(_this.spanRc);
		return;
	};
	
	Lesson.prototype.drawBg = function (callback) {
		var canvas = document.getElementById('firstArrow');
		var c = canvas.getContext('2d');
		canvas.width = 875;
		canvas.height = 150;
		var bg = new Image();
		bg.src = "./images/sprite.png";
		bg.onload = function () {
			c.drawImage(bg, 0, canvas.height / 2, canvas.width, canvas.height / 2);
		};
		setTimeout(callback, 200);
		return;
	};
	
	return Lesson;
}());
var lesson = new Lesson();
$(document).ready(lesson.drawBg);